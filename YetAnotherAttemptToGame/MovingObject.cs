﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace YetAnotherAttemptToGame
{
    public class MovingObject
    {
        private static Vector Gravity => new Vector(0, Constants.Gravity);

        public MovingObject(Rectangle rectangle, Map map = null)
        {
            MainRectangle = rectangle;
            _state = State.Falling;
            Map = map;
        }

        private IEnumerable<Vector> TopIntersectedTiles => Map.Intersections(TopScanner);
        public IEnumerable<Vector> BottomIntersectedTiles => Map.Intersections(BottomScanner);
        private IEnumerable<Vector> RightIntersectedTiles => Map.Intersections(RightScanner);
        private IEnumerable<Vector> LeftIntersectedTiles => Map.Intersections(LeftScanner);

        protected IEnumerable<Vector> AllIntersectedTiles => TopIntersectedTiles.Concat(BottomIntersectedTiles)
                                                                             .Concat(RightIntersectedTiles)
                                                                             .Concat(LeftIntersectedTiles);

        private bool TopIntersection => TopIntersectedTiles.Any();
        private bool RightIntersection => RightIntersectedTiles.Any();
        private bool BottomIntersection => BottomIntersectedTiles.Any();
        private bool LeftIntersection => LeftIntersectedTiles.Any();

        private Vector _horizontalAcceleration = Vector.Zero;
        private Vector _verticalAcceleration = Vector.Zero;


        private Vector Update(int dt)
        {
            var oldSpeed = _speed;
            if (_speed.X == 0)
                _horizontalAcceleration = Vector.Zero;
            if (_speed.Y == 0 || _speed.Y < -10)
                _verticalAcceleration = Vector.Zero;
            _speed = _speed + (Gravity + _horizontalAcceleration + _verticalAcceleration) * dt;
            return (oldSpeed + _speed) / 2 * dt;

        }

        public void Jump()
        {
            if (_state == State.Falling || _state == State.AtFloor)
                return;
            _verticalAcceleration = new Vector(0, -1);
            _speed += new Vector(0, -Constants.JumpSpeed);
        }

        public void MoveRight()
        {
            if (_state == State.MovingRight)
                return;
            _horizontalAcceleration = new Vector(-Constants.HorizontalAcceleration, 0);
            _speed += new Vector(Constants.HorizontalSpeed, 0);

        }

        public void MoveLeft()
        {
            if (_state == State.MovingLeft)
                return;
            _horizontalAcceleration = new Vector(Constants.HorizontalAcceleration, 0);
            _speed += new Vector(-Constants.HorizontalSpeed, 0);
        }

        public void UpdatePosition(int dt) => Move(Update(dt));

        private void Move(Vector shift)
        {
            var xAmount = Math.Abs(shift.X);
            var yAmount = Math.Abs(shift.Y);
            var xShift = xAmount == 0 ? 0 : shift.X / xAmount;
            var yShift = yAmount == 0 ? 0 : shift.Y / yAmount;
            for (var i = 0; i < xAmount; i++)
            {
                if (!(xShift > 0 ? RightIntersection : LeftIntersection))
                {

                    _state = xShift > 0 ? State.MovingRight : State.MovingLeft;
                    MainRectangle.X += xShift;

                }
                else
                {
                    _speed = new Vector(0, _speed.Y);
                    _state = State.Standing;
                    break;
                }

            }

            for (var i = 0; i < yAmount; i++)
            {
                if (!(yShift > 0 ? BottomIntersection : TopIntersection))
                {
                    _state = State.Falling;
                    MainRectangle.Y += yShift;
                }
                else
                {
                    _speed = new Vector(_speed.X, 0);
                    _state = BottomIntersection ? State.Standing : State.AtFloor;
                    break;
                }
            }
        }

        private Vector _speed = Vector.Zero;

        public Rectangle MainRectangle;

        public Rectangle LeftScanner => new Rectangle(MainRectangle.Left - 1, MainRectangle.Top, 1, MainRectangle.Height);
        public Rectangle RightScanner => new Rectangle(MainRectangle.Right, MainRectangle.Top, 1, MainRectangle.Height);
        public Rectangle TopScanner => new Rectangle(MainRectangle.Left, MainRectangle.Top - 1, MainRectangle.Width, 1);
        public Rectangle BottomScanner => new Rectangle(MainRectangle.Left, MainRectangle.Bottom, MainRectangle.Width, 1);

        private State _state;

        protected readonly Map Map;

    }

    public enum State
    {
        Falling, Standing, MovingRight, MovingLeft, AtFloor
    }
}
