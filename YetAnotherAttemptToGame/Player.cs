﻿using System.Drawing;
using System.Linq;

namespace YetAnotherAttemptToGame
{
    public class Player : MovingObject
    {
        public Player(Rectangle rectangle, Map map = null) : base(rectangle, map)
        {
            //
        }

        public bool IsWinner => IsOnTile(Tile.Win);

        public bool IsOnSharper => IsOnTile(Tile.Sharp);

        public bool IsOnGround => IsOnTile(Tile.Wall) && BottomIntersectedTiles.Any();

        private bool IsOnTile(Tile tile) => AllIntersectedTiles.Any(p => Map.Tiles[p.X, p.Y] == tile);

        public bool IsDead => IsOnSharper || IsOnTile(Tile.Dark);

        public Player Copy()
        {
            return new Player(MainRectangle,Map);
        }

    }
}