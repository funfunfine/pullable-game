﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace YetAnotherAttemptToGame
{
    public partial class MainMenu : Form
    {
        private Map _currentMap;
        private string _currentMapName;
        private Dictionary<string, Map> _maps;

        public MainMenu()
        {
            InitializeComponent();
            Constants.TileSize = 16;
            _maps = UpdateMaps();
            _currentMap = _maps[_maps.Keys.First()];
            _currentMapName = _maps.Keys.First();
            Levels.SelectedItem = _currentMapName;
            Shown += (_, __) => UpdateMaps();
        }


        private Dictionary<string, Map> UpdateMaps()
        {
            var maps =
                Directory.GetFiles(ConstructorForm.PathToLevels, "*", SearchOption.TopDirectoryOnly)
                         .Select(f => new {Name = f.Split('\\').Last(), Path = f})
                         .Select(f => new {f.Name, Map = BinarySerialization.ReadFromBinaryFile<Map>(f.Path)})
                         .ToDictionary(f => f.Name, p => p.Map);
            _maps = maps;
            Levels.Items.AddRange(maps.Keys.ToArray());
            return maps;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            var gameModel = new GameModel(_maps[Levels.SelectedItem.ToString()]);
            var form = new GameForm(gameModel, this);
            Hide();
            form.Show();
        }
    }
}