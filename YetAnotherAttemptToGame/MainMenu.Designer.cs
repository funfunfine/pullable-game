﻿namespace YetAnotherAttemptToGame
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.Levels = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CreateButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.LevelButton = new System.Windows.Forms.Button();
            this.PlayButton = new System.Windows.Forms.Button();
            this.Leaderboard = new System.Windows.Forms.TextBox();
            this.MenuPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuPanel
            // 
            this.MenuPanel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.MenuPanel.Controls.Add(this.Levels);
            this.MenuPanel.Controls.Add(this.CreateButton);
            this.MenuPanel.Controls.Add(this.ExitButton);
            this.MenuPanel.Controls.Add(this.LevelButton);
            this.MenuPanel.Controls.Add(this.PlayButton);
            this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuPanel.Location = new System.Drawing.Point(0, 0);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(235, 709);
            this.MenuPanel.TabIndex = 0;
            this.MenuPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Levels
            // 
            this.Levels.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.Levels.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Levels.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.4F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)), true);
            this.Levels.ForeColor = System.Drawing.Color.Gold;
            this.Levels.FormattingEnabled = true;
            this.Levels.ItemHeight = 23;
            this.Levels.Location = new System.Drawing.Point(14, 240);
            this.Levels.Name = "Levels";
            this.Levels.Size = new System.Drawing.Size(124, 23);
            this.Levels.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.pixil_frame_0__1_;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.Leaderboard);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(235, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(700, 709);
            this.panel1.TabIndex = 1;
            // 
            // CreateButton
            // 
            this.CreateButton.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.create1;
            this.CreateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CreateButton.FlatAppearance.BorderSize = 0;
            this.CreateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateButton.Location = new System.Drawing.Point(12, 390);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(125, 75);
            this.CreateButton.TabIndex = 3;
            this.CreateButton.UseVisualStyleBackColor = true;
            // 
            // ExitButton
            // 
            this.ExitButton.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.pixil_frame_0;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.FlatAppearance.BorderSize = 0;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.Location = new System.Drawing.Point(12, 590);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(125, 75);
            this.ExitButton.TabIndex = 2;
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // LevelButton
            // 
            this.LevelButton.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.level;
            this.LevelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LevelButton.FlatAppearance.BorderSize = 0;
            this.LevelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LevelButton.Location = new System.Drawing.Point(14, 145);
            this.LevelButton.Name = "LevelButton";
            this.LevelButton.Size = new System.Drawing.Size(125, 75);
            this.LevelButton.TabIndex = 1;
            this.LevelButton.UseVisualStyleBackColor = true;
            // 
            // PlayButton
            // 
            this.PlayButton.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.play1;
            this.PlayButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlayButton.FlatAppearance.BorderSize = 0;
            this.PlayButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayButton.Location = new System.Drawing.Point(12, 12);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(125, 75);
            this.PlayButton.TabIndex = 0;
            this.PlayButton.UseVisualStyleBackColor = true;
            this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
            // 
            // Leaderboard
            // 
            this.Leaderboard.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.Leaderboard.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Leaderboard.ForeColor = System.Drawing.Color.Gold;
            this.Leaderboard.Location = new System.Drawing.Point(12, 9);
            this.Leaderboard.Multiline = true;
            this.Leaderboard.Name = "Leaderboard";
            this.Leaderboard.Size = new System.Drawing.Size(212, 668);
            this.Leaderboard.TabIndex = 0;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 709);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MenuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainMenu";
            this.Text = "RUN";
            this.MenuPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button LevelButton;
        private System.Windows.Forms.Button PlayButton;
        private System.Windows.Forms.ListBox Levels;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox Leaderboard;
    }
}