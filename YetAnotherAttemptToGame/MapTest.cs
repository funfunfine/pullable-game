﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace YetAnotherAttemptToGame
{   
    [TestFixture]
    class MapTest
    {
        

        [Test]
        public void CreateWalls()
        {
            var map = new Map(50,50);
            map.AddWalls(new Rectangle(10, 10, 10, 10), new Rectangle(20, 20, 20, 20));
            Assert.AreEqual("_____\n_W___\n__WW_\n__WW_\n_____",map.ToString());
        }

        [Test]
        public void FullRectangles()
        {
            var map = new Map(50,50);
            Assert.AreEqual(25,map.AllTiles.Count());
        }

        [TestCase(2,2,1,4)]
        [TestCase(2,2,2,12)]
        [TestCase(2,2,3,20)]
        public void GetLocals(int x, int y, int w, int l)
        {
            var map = new Map(50,50);
            var locals = map.GetLocalTiles(new Point(x, y), w);
            Assert.AreEqual(l,locals.Count());
        }

        [TestCase(0,0,9,9,0,0)]
        [TestCase(5,5,9,9, 1,1,0,0,0,1,1,0)]
        [TestCase(15,15,20,20, 1,1, 2,2, 3,3, 2,1, 3,1, 3,2, 1,2, 1,3, 2,3)]
        [TestCase(10,10,9,9,1,1)]
        [TestCase(20,20,19,19,2,2,2,3,3,2,3,3)]
        [TestCase(5,12,10,3,0,1,1,1)]
        [TestCase(5,35,40,10, 0,3, 0,4, 1,3, 1,4, 2, 3, 2, 4, 3, 3, 3, 4, 4, 3, 4, 4 )]       
        public void TilesOfRectangle(int x, int y, int w, int h, params int[] p)
        {
            var rectangle = new Rectangle(x,y,w,h);
            var tiles = Map.TilesUnderRectangle(rectangle).ToList();
            Assert.IsTrue(tiles.Count()==p.Length/2);
            for (var i = 0; i < p.Length-1; i+=2)
            {
                x = p[i];
                y = p[i + 1];
                Assert.IsTrue(tiles.Contains(new Vector(x,y)));
            }                     
        }

        [TestCase(25,5,10,10,false)]
        [TestCase(5,5,10,10,true)]
        [TestCase(5,25,10,10,true)]
        [TestCase(20,30,10,10,true)]
        [TestCase(20,30,10,10,true)]
        [TestCase(5,12,10,3,false)]
        [TestCase(5,7,10,3,true)]
        [TestCase(5,5,10,1,true)]
        [TestCase(5,5,1,10,true)]
        public void AnyIntersection(int x, int y, int w, int h,bool result)
        {
            var map = new Map(50,50, new Rectangle(0,0, 10,10), new Rectangle(0,30,40,10));
            var testRectangle = new Rectangle(x,y,w,h);
            Assert.AreEqual(result,map.AnyIntersection(testRectangle));
        }

        [TestCase(-1, -1, 1, 10, true)]
        [TestCase(51, 52, 1, 10, true)]
        public void BorderIntersection(int x, int y, int w, int h, bool result)
        {
            var map = new Map(50, 50, new Rectangle(0, 0, 10, 10), new Rectangle(0, 30, 40, 10));
            var testRectangle = new Rectangle(x, y, w, h);
            Assert.AreEqual(result, map.AnyIntersection(testRectangle));
        }

        [TestCase(10, 10, 1, 1)]
        [TestCase(5, 5, 0, 0)]
        [TestCase(15, 15, 1, 1)]
        [TestCase(13, 21, 1, 2)]
        public void ConvertCoordinates(int x0, int y0,int x, int y)
        {
            var map = new Map(100,100);
            var point = new Vector(x0,y0);
            Assert.AreEqual(new Vector(x,y), Map.ConvertToTiles(point));
            

        }

        [Test]
        public void AddWall()
        {
            var map = new Map(10, 10);
            map.AddWalls(new Rectangle(0,0,10,10));
            Assert.AreEqual("W",map.ToString());
        }

    }
}
