﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace YetAnotherAttemptToGame
{
     internal sealed class GameForm : Form
    {
        private static readonly string ImagesPath =Environment.CurrentDirectory+ @"\Images\";

        private readonly GameModel _game;

        private readonly HashSet<Keys> _keysPressed = new HashSet<Keys>();


        private readonly Dictionary<Tile, List<Image>> _tileImages = new Dictionary<Tile, List<Image>>
        {
            [Tile.Wall] = new List<Image>(),
            [Tile.Sharp] = new List<Image>(),
            [Tile.Win] = new List<Image>(),
            [Tile.Dark] = new List<Image>()
        };

        private readonly Timer _timer = new Timer { Interval = 15 };
        private Image _darkImage;

        private int Time = 0;

        private Dictionary<Keys, Action> _keyMovements;
        private readonly MapDrawer _mapDrawer;
        private readonly Form _parent;

        private Image _playerImage;

        private Image _trayImage;

        public GameForm(GameModel model, MainMenu parent = null)
        {
            _game = model;          
            _parent = parent;            

            PrepareImages();
            _mapDrawer = new MapDrawer(_game.Map);

            InitializeKeyMovements();
            InitializeForm();
            _timer.Start();
        }


        private void PrepareImages()
        {
            _trayImage = Image.FromFile(ImagesPath + "tray.png");
            _playerImage = Image.FromFile(ImagesPath + "player.png");
            _darkImage = Image.FromFile(ImagesPath + $"dark.png");
        }

        private void InitializeForm()
        {
            _timer.Tick += TimerTick;
            Paint += DrawGame;
            ClientSize = new Size(_game.Map.Width, _game.Map.Height);
            DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.None;

        }

        private void InitializeKeyMovements()
        {
            _keyMovements = new Dictionary<Keys, Action>
            {
                [Keys.Space] = _game.Jump,
                [Keys.A] = _game.MoveLeft,
                [Keys.D] = _game.MoveRight,
                [Keys.R] =()=>
                {
                    Time = 0;
                    _game.Restart();
                }
            };
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B ||e.KeyCode== Keys.Escape)
            {
                _parent.Show();
                Close();
            }
            if (_keyMovements.ContainsKey(e.KeyCode))
                _keysPressed.Add(e.KeyCode);
        }

        protected override void OnKeyUp(KeyEventArgs e) => _keysPressed.Remove(e.KeyCode);

        private void DrawGame(object sender, PaintEventArgs e)
        {

            var g = e.Graphics;
            var shouldContinue = CheckGameState(g);
            if (!shouldContinue)
                return;
            Draw(g);

        }

        private bool CheckGameState(Graphics g)
        {
            switch (_game.State)
            {
                case GameModel.GameState.Won:
                {
                    _game.Map.BestTime = Time;
                    DrawWon(g);
                }
                    return false;
                case GameModel.GameState.Lost:
                    DrawLost(g);
                    return false;
            }

            return true;
        }

        private void DrawLost(Graphics g)
        {
            var image = Image.FromFile(ImagesPath + "lose.jpg");
            g.DrawImage(image, ClientRectangle);
        }

        private void DrawWon(Graphics g)
        {
            var image = Image.FromFile(ImagesPath + "win.jpg");
            g.DrawImage(image, ClientRectangle);
        }


        private void Draw(Graphics g)
        {
            _mapDrawer.DrawField(g,ClientRectangle,0);
            DrawPlayer(g);
            DrawTray(g);
            DrawDark(g);

        }

        private void DrawDark(Graphics g)
        {
            var darkers = _game.Map.TileRectangles(Tile.Dark).ToArray();
            if (darkers.Length <= 0)
                return;
            foreach (var rectangle in darkers)
                g.DrawImage(_darkImage, rectangle);
        }

        private void DrawPlayer(Graphics g)
        {
            g.DrawImage(_playerImage,_game.Player.MainRectangle);
        }


        private void DrawTray(Graphics g)
        {            
            var trayCount = _game.Tray.Count;
            if (trayCount <= 1)
                return;
            foreach (var point in _game.Tray)
            {
                var rectangle = new Rectangle((point.ToVector() * Constants.TileSize).ToPoint(), Map.TileSize);
                rectangle.Inflate(Constants.InflateSize);
                g.DrawImage(_trayImage,rectangle);   
            }
            
        }


        private void TimerTick(object sender, EventArgs e)
        {
            foreach (var key in _keysPressed)
                _keyMovements[key]();
            _game.OneIteration();
            Time++;
            Invalidate();
        }
    }
}