﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace YetAnotherAttemptToGame
{
    internal class MapDrawer
    {
        private static readonly string ImagesPath = Environment.CurrentDirectory + @"\Images\";

        private readonly Map _map;

        private readonly Lazy<Dictionary<Tile, Rectangle[]>> _tileArrays;

        private readonly Dictionary<Tile, List<Image>> _tileImages = new Dictionary<Tile, List<Image>>
        {
            [Tile.Wall] = new List<Image>(),
            [Tile.Sharp] = new List<Image>(),
            [Tile.Win] = new List<Image>(),
            [Tile.Dark] = new List<Image>()
        };

        public MapDrawer(Map map)
        {
            _map = map;
            PrepareImages();
            _tileArrays = new Lazy<Dictionary<Tile, Rectangle[]>>(TileArraysFactory);
        }

        private void PrepareImages()
        {
            _tileImages[Tile.Wall].Add(Image.FromFile(ImagesPath + "wall.png"));
            _tileImages[Tile.Wall].Add(Image.FromFile(ImagesPath + "wall_edge.png"));

            _tileImages[Tile.Sharp].Add(Image.FromFile(ImagesPath + $"sharp.png"));
            _tileImages[Tile.Sharp].Add(Image.FromFile(ImagesPath + $"sharp.png"));


            _tileImages[Tile.Win].Add(Image.FromFile(ImagesPath + $"win.png"));
            _tileImages[Tile.Win].Add(Image.FromFile(ImagesPath + $"win_edge.png"));

            _tileImages[Tile.Dark].Add(Image.FromFile(ImagesPath + $"dark.png"));
            _tileImages[Tile.Dark].Add(Image.FromFile(ImagesPath + $"dark.png"));
        }


        public void DrawField(Graphics g, Rectangle drawWindow, int menuPanelWidth)
        {
            DrawBackground(g, drawWindow);
            foreach (var key in _tileImages.Keys.Where(t => t != Tile.Dark))
                DrawTiles(g, key, menuPanelWidth);
        }

        private Dictionary<Tile, Rectangle[]> TileArraysFactory()
        {
            var result = new Dictionary<Tile, Rectangle[]>();
            for (var i = 0; i < 5; i++)
            {
                var tile = (Tile) i;
                result[tile] = _map.TileRectangles(tile).ToArray();
            }

            return result;
        }

        private static void DrawBackground(Graphics g, Rectangle clientRectangle)
        {
            var brush = new LinearGradientBrush(clientRectangle, Color.CornflowerBlue, Color.AliceBlue,
                LinearGradientMode.Horizontal);
            g.FillRectangle(brush, clientRectangle);
        }

        private void DrawTiles(Graphics g, Tile tile, int shift = 0)
        {
            var tiles = _tileArrays.Value[tile];
            var images = _tileImages[tile];
            if (tiles.Length <= 0)
                return;
            foreach (var rectangle in tiles)
            {
                var image = _map.AnyOtherTiles(Tile.None, rectangle) || _map.AnyOtherTiles(Tile.Dark, rectangle)
                                ? images[1]
                                : images[0];
                var rect = new Rectangle(rectangle.X + shift, rectangle.Y, rectangle.Width, rectangle.Height);
                rect.Inflate(Constants.InflateSize);
                g.DrawImage(image, rect);
            }
        }
    }
}