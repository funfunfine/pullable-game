﻿namespace YetAnotherAttemptToGame
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Levels = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TileSize = new System.Windows.Forms.TextBox();
            this.GameHeight = new System.Windows.Forms.TextBox();
            this.GameWidth = new System.Windows.Forms.TextBox();
            this.Constructor = new System.Windows.Forms.Button();
            this.Game = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.Levels);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TileSize);
            this.groupBox1.Controls.Add(this.GameHeight);
            this.groupBox1.Controls.Add(this.GameWidth);
            this.groupBox1.Controls.Add(this.Constructor);
            this.groupBox1.Controls.Add(this.Game);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(363, 560);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.exit;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(17, 452);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 51);
            this.button1.TabIndex = 21;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // Levels
            // 
            this.Levels.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.Levels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Levels.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Levels.FormattingEnabled = true;
            this.Levels.Location = new System.Drawing.Point(6, 244);
            this.Levels.Name = "Levels";
            this.Levels.Size = new System.Drawing.Size(176, 21);
            this.Levels.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(163, 274);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Tilesize";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(69, 274);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "Height";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(8, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Width";
            // 
            // TileSize
            // 
            this.TileSize.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.TileSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TileSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.3F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TileSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.TileSize.Location = new System.Drawing.Point(169, 300);
            this.TileSize.Margin = new System.Windows.Forms.Padding(0);
            this.TileSize.Name = "TileSize";
            this.TileSize.Size = new System.Drawing.Size(34, 19);
            this.TileSize.TabIndex = 16;
            this.TileSize.Text = "16";
            // 
            // GameHeight
            // 
            this.GameHeight.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.GameHeight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GameHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.3F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GameHeight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GameHeight.Location = new System.Drawing.Point(75, 300);
            this.GameHeight.Margin = new System.Windows.Forms.Padding(0);
            this.GameHeight.Name = "GameHeight";
            this.GameHeight.Size = new System.Drawing.Size(47, 19);
            this.GameHeight.TabIndex = 15;
            this.GameHeight.Text = "1000";
            // 
            // GameWidth
            // 
            this.GameWidth.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.GameWidth.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GameWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.3F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GameWidth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GameWidth.Location = new System.Drawing.Point(14, 301);
            this.GameWidth.Margin = new System.Windows.Forms.Padding(0);
            this.GameWidth.Name = "GameWidth";
            this.GameWidth.Size = new System.Drawing.Size(42, 19);
            this.GameWidth.TabIndex = 14;
            this.GameWidth.Text = "1000";
            // 
            // Constructor
            // 
            this.Constructor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Constructor.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.create1;
            this.Constructor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Constructor.FlatAppearance.BorderSize = 0;
            this.Constructor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Constructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Constructor.Location = new System.Drawing.Point(17, 335);
            this.Constructor.Name = "Constructor";
            this.Constructor.Size = new System.Drawing.Size(93, 51);
            this.Constructor.TabIndex = 12;
            this.Constructor.UseVisualStyleBackColor = false;
            // 
            // Game
            // 
            this.Game.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.Game.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.play1;
            this.Game.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Game.FlatAppearance.BorderSize = 0;
            this.Game.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Game.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.Game.Location = new System.Drawing.Point(20, 48);
            this.Game.Name = "Game";
            this.Game.Size = new System.Drawing.Size(104, 63);
            this.Game.TabIndex = 11;
            this.Game.UseVisualStyleBackColor = false;
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(970, 560);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StartForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox Levels;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TileSize;
        private System.Windows.Forms.TextBox GameHeight;
        private System.Windows.Forms.TextBox GameWidth;
        private System.Windows.Forms.Button Constructor;
        private System.Windows.Forms.Button Game;
    }
}